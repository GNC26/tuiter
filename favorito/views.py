# Create your views here.
from django.shortcuts import render_to_response
from django.template import RequestContext, Context
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from perfil.models import Tuit
from django.contrib.auth.models import User
from perfil.models import Favorito
import datetime

def home(request):
    context = RequestContext(request, {
        'favoritos': request.user.favorito_related.order_by('-fecha')
	})
    return render_to_response("favorito.html", context)

def favorito(request):
    if request.method == 'POST':
        favorito = Favorito()
        favorito.fecha = datetime.datetime.now()
        favorito.tuit = Tuit.objects.get(pk = request.POST.get("id-tuit"))
        favorito.usuario = request.user
        favorito.save()
    return HttpResponseRedirect('/perfil/', RequestContext(request))

def eliminar(request):
    Favorito.objects.get(tuit = request.POST.get('id-tuit')).delete()
    return HttpResponseRedirect('/perfil/', RequestContext(request))


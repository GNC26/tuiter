from django.shortcuts import render_to_response
from django.template import RequestContext
from perfil.models import Seguimiento, Tuit

def home(request):
	seguimiento_usuario = Seguimiento.objects.filter(seguidor = request.user)
	los_que_sigue = []
	for seguimiento in seguimiento_usuario: #request.user.seguidos.all():
		los_que_sigue.append(seguimiento.seguido)
	tuits = Tuit.objects.filter(usuario__in = los_que_sigue).order_by('-fecha')
	return render_to_response('tl.html', RequestContext(request, {
		'tuits': tuits
	}))

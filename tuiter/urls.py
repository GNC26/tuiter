from django.conf.urls import patterns, include, url
import usuario.views
import perfil.views
import favorito.views
import tl.views

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
	# Examples:
	# url(r'^$', 'tuiter.views.home', name='home'),
	# url(r'^tuiter/', include('tuiter.foo.urls')),

	# Uncomment the admin/doc line below to enable admin documentation:
	# url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

	# Uncomment the next line to enable the admin:
	# url(r'^admin/', include(admin.site.urls)),
	url(r'^$', usuario.views.home),
	url(r'^usuario/login/$', usuario.views.login),
	url(r'^usuario/logout/$', usuario.views.logout),
	url(r'^usuario/signup/$', usuario.views.signup),
	url(r'^perfil/$', perfil.views.home),
	url(r'^perfil/tuitear/$', perfil.views.tuitear),
	url(r'^usuario/cambiarcontrasenia/$', usuario.views.cambiarcontrasenia),
	url(r'^usuario/resetpass/$', usuario.views.resetpass),
	url(r'^perfil/seguir/$', perfil.views.seguir),
	url(r'^perfil/dejar-seguir/$', perfil.views.dejar),
	url(r'^favoritos/$', favorito.views.home),
	url(r'^favorito/agregar/$', favorito.views.favorito),
	url(r'^favorito/quitar/$', favorito.views.eliminar),
	url(r'^timeline/$', tl.views.home),
	url(r'^([aA-zZ]+)/$', perfil.views.usuario),
)

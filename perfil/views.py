from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from perfil.models import Tuit, Seguimiento
from django.contrib.auth.models import User, auth
import datetime

@login_required
def home(request):
	context = RequestContext(request, {
		'tuits': request.user.tuits.order_by('-id'),
        'tuits_favoritos': Tuit.favoritos_de(request.user)
	})
	return render_to_response('perfil.html', context)

@login_required
def tuitear(request):
	if request.method == 'POST':
		tuit = Tuit()
		tuit.mensaje = request.POST.get('tuit')
		tuit.usuario = request.user
		tuit.fecha = datetime.datetime.now()
		tuit.save()
	return HttpResponseRedirect('/perfil/')


@login_required
def usuario(request, nombre_usuario):
	try:
		usuario_perfil = User.objects.get(username = nombre_usuario)
		seguimiento = Seguimiento.objects.filter(seguidor = request.user, seguido = usuario_perfil)
		context = RequestContext(request, {
			'tuits': usuario_perfil.tuits.order_by('-id'),
			'seguimiento': seguimiento,
			'usuario_perfil': usuario_perfil
		})
		return render_to_response('perfil_3ro.html', context)
	except Exception as e:
		pass
	return HttpResponseRedirect('/perfil/')

def seguir(request):
	seguido = User.objects.get(pk = request.POST.get('seguido'))
	if seguido.username == request.user.username:
		raise Exception('No puedes hacer esto')
	seguimiento = Seguimiento()
	seguimiento.seguido = seguido
	seguimiento.seguidor = request.user
	seguimiento.fecha = datetime.datetime.now()
	seguimiento.save()
	return HttpResponseRedirect('/%s/' % seguido.username)

def dejar(request):
	seguido = User.objects.get(pk = request.POST.get('seguido'))
	Seguimiento.objects.filter(seguidor = request.user, seguido = seguido).delete()
	return HttpResponseRedirect('/%s/' % seguido.username)
from django.db import models
from django.contrib.auth.models import User

class Tuit(models.Model):
	id = models.AutoField(db_column = 'id', primary_key = True)
	mensaje = models.CharField(db_column = 'mensaje', max_length = 255)
	usuario = models.ForeignKey(User, db_column = 'usuario', related_name = 'tuits')
	fecha = models.DateTimeField(db_column = 'fecha')

	@staticmethod
	def favoritos_de(user):
		return Favorito.objects.filter(usuario = user).values_list('tuit', flat = True)

	class Meta:
		db_table = 'tuit'


class Favorito(models.Model):
	id = models.AutoField(db_column = 'id', primary_key = True)
	fecha = models.DateTimeField(db_column = 'fecha')
	tuit = models.ForeignKey(Tuit, db_column = 'tuit')
	usuario = models.ForeignKey(User, db_column = 'usuario', related_name = 'favorito_related')
	class Meta:
		db_table = 'favorito'
		unique_together = ('tuit', 'usuario')

class Seguimiento(models.Model):
	id = models.AutoField(db_column= 'id', primary_key = True)
	seguido = models.ForeignKey(User,db_column = 'seguido', related_name = 'seguidores')
	seguidor = models.ForeignKey(User, db_column = 'seguidor', related_name = 'seguidos')
	fecha = models.DateTimeField (db_column = 'fecha')
	class Meta:
		db_table = 'seguimiento'
		unique_together = ('seguido','seguidor')

class Retuit(models.Model):
	id = models.AutoField(db_column = 'id', primary_key = True)
	usuario = models.ForeignKey(User, db_column= 'usuario')
	tuit = models.ForeignKey(Tuit, db_column ='tuit')
	fecha = models.DateTimeField(db_column = 'fecha')
	class Meta:
		db_table = 'retuit'
		unique_together = ('usuario','tuit')


